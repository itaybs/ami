<?php

namespace app\controllers;

use Yii;
use app\models\SubscriptionDepartment;
use app\models\SubscriptionDepartmentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UnauthorizedHttpException;
use app\models\Specialization;

/**
 * SubscriptionDepartmentController implements the CRUD actions for SubscriptionDepartment model.
 */
class SubscriptionDepartmentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SubscriptionDepartment models.
     * @return mixed
     */
    public function actionIndex()
    {	
	if (!\Yii::$app->user->can('indexMemberActivity'))
			throw new UnauthorizedHttpException ('Hey, You are not allowed to view Member Activity');
        $searchModel = new SubscriptionDepartmentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'Specializations' => Specialization::getSpecializationsWithAllSpecializations(),
			'CodeOfActivity' => $searchModel->CodeOfActivity,
        ]);
    }

    /**
     * Displays a single SubscriptionDepartment model.
     * @param integer $CodeOfActivity
     * @param integer $Id
     * @return mixed
     */
    public function actionView($CodeOfActivity, $Id)
    {
        return $this->render('view', [
            'model' => $this->findModel($CodeOfActivity, $Id),
        ]);
    }

    /**
     * Creates a new SubscriptionDepartment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SubscriptionDepartment();
		if (!\Yii::$app->user->can('createMemberActivity'))
			throw new UnauthorizedHttpException ('Hey, You are not allowed to create Member Activity');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'CodeOfActivity' => $model->CodeOfActivity, 'Id' => $model->Id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SubscriptionDepartment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $CodeOfActivity
     * @param integer $Id
     * @return mixed
     */
    public function actionUpdate($CodeOfActivity, $Id)
    {
        $model = $this->findModel($CodeOfActivity, $Id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'CodeOfActivity' => $model->CodeOfActivity, 'Id' => $model->Id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SubscriptionDepartment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $CodeOfActivity
     * @param integer $Id
     * @return mixed
     */
    public function actionDelete($CodeOfActivity, $Id)
    {	
		if (!\Yii::$app->user->can('deleteMemberActivity'))
			throw new UnauthorizedHttpException ('Hey, You are not allowed to delete Member Activity');
        $this->findModel($CodeOfActivity, $Id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SubscriptionDepartment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $CodeOfActivity
     * @param integer $Id
     * @return SubscriptionDepartment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($CodeOfActivity, $Id)
    {
        if (($model = SubscriptionDepartment::findOne(['CodeOfActivity' => $CodeOfActivity, 'Id' => $Id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
