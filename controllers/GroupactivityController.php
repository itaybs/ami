<?php

namespace app\controllers;

use Yii;
use app\models\Groupactivity;
use app\models\GroupactivitySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UnauthorizedHttpException;
use app\models\Specialization;

/**
 * GroupactivityController implements the CRUD actions for Groupactivity model.
 */
class GroupactivityController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Groupactivity models.
     * @return mixed
     */
    public function actionIndex()
    {	
	if (!\Yii::$app->user->can('indexGroupActivity'))
			throw new UnauthorizedHttpException ('Hey, You are not allowed to view Group Activity');
        $searchModel = new GroupactivitySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'Specializations' => Specialization::getSpecializationsWithAllSpecializations(),
			'CodeOfActivity' => $searchModel->CodeOfActivity,
        ]);
    }

    /**
     * Displays a single Groupactivity model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {	
		if (!\Yii::$app->user->can('viewGroupActivity'))
			throw new UnauthorizedHttpException ('Hey, You are not allowed to view Group Activity');
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Groupactivity model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Groupactivity();
		if (!\Yii::$app->user->can('createGroupActivity'))
			throw new UnauthorizedHttpException ('Hey, You are not allowed to create Group Activity');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->CodeOfActivity]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Groupactivity model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		if (!\Yii::$app->user->can('updateGroupActivity'))
			throw new UnauthorizedHttpException ('Hey, You are not allowed to update Group Activity');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->CodeOfActivity]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Groupactivity model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        
		if (!\Yii::$app->user->can('deleteActivity'))
			throw new UnauthorizedHttpException ('Hey, You are not allowed to delete Group Activity');
		$this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Groupactivity model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Groupactivity the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Groupactivity::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
