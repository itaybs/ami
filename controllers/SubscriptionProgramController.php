<?php

namespace app\controllers;

use Yii;
use app\models\SubscriptionProgram;
use app\models\SubscriptionProgramSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UnauthorizedHttpException;
use app\models\Program;

/**
 * SubscriptionProgramController implements the CRUD actions for SubscriptionProgram model.
 */
class SubscriptionProgramController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SubscriptionProgram models.
     * @return mixed
     */
    public function actionIndex()
    {	
		if (!\Yii::$app->user->can('indexMemberProgram'))
			throw new UnauthorizedHttpException ('Hey, You are not allowed to view Members Programs');
        $searchModel = new SubscriptionProgramSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'AllPrograms' => Program::getAllProgramsWithAllAllPrograms(),
			'NumOfProgram' => $searchModel->NumOfProgram
        ]);
    }

    /**
     * Displays a single SubscriptionProgram model.
     * @param integer $NumOfProgram
     * @param integer $Id
     * @return mixed
     */
    public function actionView($NumOfProgram, $Id)
    {
        return $this->render('view', [
            'model' => $this->findModel($NumOfProgram, $Id),
        ]);
    }

    /**
     * Creates a new SubscriptionProgram model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SubscriptionProgram();
		if (!\Yii::$app->user->can('createMemberProgram'))
			throw new UnauthorizedHttpException ('Hey, You are not allowed to create Member Program');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'NumOfProgram' => $model->NumOfProgram, 'Id' => $model->Id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SubscriptionProgram model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $NumOfProgram
     * @param integer $Id
     * @return mixed
     */
    public function actionUpdate($NumOfProgram, $Id)
    {
        $model = $this->findModel($NumOfProgram, $Id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'NumOfProgram' => $model->NumOfProgram, 'Id' => $model->Id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SubscriptionProgram model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $NumOfProgram
     * @param integer $Id
     * @return mixed
     */
    public function actionDelete($NumOfProgram, $Id)
    {	
	
		if (!\Yii::$app->user->can('deleteMemberProgram'))
			throw new UnauthorizedHttpException ('Hey, You are not allowed to delete Member Program');
        $this->findModel($NumOfProgram, $Id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SubscriptionProgram model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $NumOfProgram
     * @param integer $Id
     * @return SubscriptionProgram the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($NumOfProgram, $Id)
    {
        if (($model = SubscriptionProgram::findOne(['NumOfProgram' => $NumOfProgram, 'Id' => $Id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
