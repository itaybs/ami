<?php

namespace app\models;

	use Yii;
	use \yii\web\IdentityInterface;
	use yii\db\ActiveRecord;
	use yii\behaviors\BlameableBehavior;
	use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "trainingdevice".
 *
 * @property integer $TrainingDeviceCode
 * @property string $Description
 */
class Trainingdevice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'training_device';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['TrainingDeviceCode', 'Description'], 'required'],
            [['TrainingDeviceCode'], 'integer'],
            [['Description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'TrainingDeviceCode' => 'Training Device Code',
            'Description' => 'Description',
        ];
    }
	
		public static function getTrainingdevices()
	{
		$allTrainingdevices = self::find()->all();
		$allTrainingdevicesArray = ArrayHelper::
					map($allTrainingdevices, 'TrainingDeviceCode', 'Description');
		return $allTrainingdevicesArray;						
	}
	
		public static function getTrainingdevicesWithAllTrainingdevices()
	{
		$allTrainingdevices = self::getTrainingdevices();
		$allTrainingdevices[-1] = 'All Training devices';
		$allTrainingdevices = array_reverse ( $allTrainingdevices, true );
		return $allTrainingdevices;	
	}
}
