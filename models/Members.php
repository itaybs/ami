<?php

namespace app\models;

	use Yii;
	use \yii\web\IdentityInterface;
	use yii\db\ActiveRecord;
	use yii\behaviors\BlameableBehavior;
	use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "members".
 *
 * @property integer $Id
 * @property string $Name
 * @property string $LastName
 * @property integer $Phone
 * @property string $DateOfBirth
 * @property integer $Email
 * @property string $City
 * @property string $Adress
 * @property integer $Type
 * @property string $DateOfStart
 */
class Members extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'members';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Id', 'Name', 'LastName', 'Phone', 'DateOfBirth', 'Email', 'City', 'Adress', 'Type', 'DateOfStart'], 'required'],
            [['Id', 'Phone',  'Type'], 'integer'],
            [['DateOfBirth', 'DateOfStart'], 'safe'],
            [['Name', 'LastName', 'Email','City', 'Adress'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
	 
	 
	public function getTypeItem()
    {
        return $this->hasOne(Typeofmember::className(), ['Type' => 'Type']);
    }
	 
	 
	 
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'Name' => 'Name',
            'LastName' => 'Last Name',
            'Phone' => 'Phone',
            'DateOfBirth' => 'Date Of Birth',
            'Email' => 'Email',
            'City' => 'City',
            'Adress' => 'Adress',
            'Type' => 'Type',
            'DateOfStart' => 'Date Of Start',
        ];
    }
	
		public static function getAllMembers()
	{
		$allMembers = self::find()->all();
		$allMembersArray = ArrayHelper::
					map($allMembers, 'Id', 'Name');
		return $allMembersArray;						
	}

	
	
}
