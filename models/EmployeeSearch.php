<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Employee;

/**
 * EmployeeSearch represents the model behind the search form about `app\models\Employee`.
 */
class EmployeeSearch extends Employee
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['EmployeeId',  'Phone','UserId', 'Specialization', 'JobCode'], 'integer'],
            [['Name', 'LastName', 'DateOfBirth', 'DateOfStartPosition'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Employee::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

		
		$this->Specialization == -1 ? $this->Specialization = null : $this->Specialization;
		$this->JobCode == -1 ? $this->JobCode = null : $this->JobCode;
		
        // grid filtering conditions
        $query->andFilterWhere([
            'EmployeeId' => $this->EmployeeId,
           'Phone' => $this->Phone,
            'DateOfBirth' => $this->DateOfBirth,
            'DateOfStartPosition' => $this->DateOfStartPosition,
            'Specialization' => $this->Specialization,
            'JobCode' => $this->JobCode,
			'UserId' => $this->UserId,
        ]);

        $query->andFilterWhere(['like', 'Name', $this->Name])
            ->andFilterWhere(['like', 'LastName', $this->LastName]);

        return $dataProvider;
    }
}
