<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SubscriptionProgram;

/**
 * SubscriptionProgramSearch represents the model behind the search form about `app\models\SubscriptionProgram`.
 */
class SubscriptionProgramSearch extends SubscriptionProgram
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NumOfProgram', 'Id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SubscriptionProgram::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

		$this->NumOfProgram == -1 ? $this->NumOfProgram = null : $this->NumOfProgram;
		
        // grid filtering conditions
        $query->andFilterWhere([
            'NumOfProgram' => $this->NumOfProgram,
            'Id' => $this->Id,
        ]);

        return $dataProvider;
    }
}
