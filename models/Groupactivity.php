<?php

namespace app\models;

	use Yii;
	use \yii\web\IdentityInterface;
	use yii\db\ActiveRecord;
	use yii\behaviors\BlameableBehavior;
	use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "groupactivity".
 *
 * @property integer $CodeOfActivity
 * @property string $Day
 * @property string $Time
 */
class Groupactivity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'group_activity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CodeOfActivity', 'Day', 'Time','nameOfActivity'], 'required'],
            [['CodeOfActivity'], 'integer'],
            [['Time'], 'safe'],
            [['Day','nameOfActivity'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
	 
	 
	 
	public function getCodeOfActivityItem()
    {
        return $this->hasOne(Specialization::className(), ['SpecializationCode' => 'CodeOfActivity']);
    }
	 
	 
	 
	 
    public function attributeLabels()
    {
        return [
            'CodeOfActivity' => 'Code Of Activity',
            'Day' => 'Day',
            'Time' => 'Time',
			'nameOfActivity' => 'name Of Activity',
        ];
    }
	
		public static function getAllActivities()
	{
		$allActivities = self::find()->all();
		$allActivitiesArray = ArrayHelper::
					map($allActivities, 'CodeOfActivity', 'nameOfActivity');
		return $allActivitiesArray;						
	}
}
