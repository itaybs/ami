<?php

namespace app\models;

	use Yii;
	use \yii\web\IdentityInterface;
	use yii\db\ActiveRecord;
	use yii\behaviors\BlameableBehavior;
	use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "specialization".
 *
 * @property integer $SpecializationCode
 * @property string $Description
 */
class Specialization extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'specialization';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['SpecializationCode', 'Description'], 'required'],
            [['SpecializationCode'], 'integer'],
            [['Description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'SpecializationCode' => 'Specialization Code',
            'Description' => 'Description',
        ];
    }
	
	public static function getSpecializations()
	{
		$allSpecializations = self::find()->all();
		$allSpecializationsArray = ArrayHelper::
					map($allSpecializations, 'SpecializationCode', 'Description');
		return $allSpecializationsArray;						
	}
	
	public static function getSpecializationsWithAllSpecializations()
	{
		$allSpecializations = self::getSpecializations();
		$allSSpecializations[-1] = 'All Specializations';
		$allSpecializations = array_reverse ( $allSpecializations, true );
		return $allSpecializations;	
	}	
	
	
}
