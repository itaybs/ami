<?php

namespace app\models;

	use Yii;
	use \yii\web\IdentityInterface;
	use yii\db\ActiveRecord;
	use yii\behaviors\BlameableBehavior;
	use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "jobs".
 *
 * @property integer $JobCode
 * @property string $Description
 */
class Jobs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jobs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['JobCode', 'Description'], 'required'],
            [['JobCode'], 'integer'],
            [['Description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'JobCode' => 'Job Code',
            'Description' => 'Description',
        ];
    }
	
		public static function getJobs()
	{
		$allJobs = self::find()->all();
		$allJobsArray = ArrayHelper::
					map($allJobs, 'JobCode', 'Description');
		return $allJobsArray;						
	}
	
	public static function getJobsWithAllJobs()
	{
		$allJobs = self::getJobs();
		$allJobs[-1] = 'All Jobs';
		$allJobs = array_reverse ( $allJobs, true );
		return $allJobs;	
	}	
	
	
}
