<?php

namespace app\models;

	use Yii;
	use \yii\web\IdentityInterface;
	use yii\db\ActiveRecord;
	use yii\behaviors\BlameableBehavior;
	use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "shift_code".
 *
 * @property integer $ShiftId
 * @property string $descripition
 */
class ShiftCode extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shift_code';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ShiftId', 'descripition'], 'required'],
            [['ShiftId'], 'integer'],
            [['descripition'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ShiftId' => 'Shift ID',
            'descripition' => 'Descripition',
        ];
    }
	
		public static function getShiftCodes()
	{
		$allShiftCodes = self::find()->all();
		$allShiftCodesArray = ArrayHelper::
					map($allShiftCodes, 'ShiftId', 'descripition');
		return $allShiftCodesArray;						
	}
	
		public static function getShiftCodesWithAllShiftCodes()
	{
		$allShiftCodes = self::getShiftCodes();
		$allShiftCodes[-1] = 'All ShiftCodes';
		$allShiftCodes = array_reverse ( $allShiftCodes, true );
		return $allShiftCodes;	
	}	
	
	
}
