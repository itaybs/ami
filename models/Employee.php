<?php

namespace app\models;

	use Yii;
	use \yii\web\IdentityInterface;
	use yii\db\ActiveRecord;
	use yii\behaviors\BlameableBehavior;
	use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "employee".
 *
 * @property integer $EmployeeId
 * @property integer $Id
 * @property string $Name
 * @property string $LastName
 * @property integer $Phone
 * @property string $DateOfBirth
 * @property string $DateOfStartPosition
 * @property integer $Specialization
 * @property integer $JobCode
 */
class Employee extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'employee';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['EmployeeId', 'Name', 'LastName', 'Phone', 'DateOfBirth', 'DateOfStartPosition', 'Specialization', 'JobCode'], 'required'],
            [['EmployeeId', 'Phone', 'Specialization', 'JobCode','UserId'], 'integer'],
            [['DateOfBirth', 'DateOfStartPosition'], 'safe'],
            [['Name', 'LastName', 'JobCode'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
	 
	 
	 
	 public function getSpecializationItem()
    {
        return $this->hasOne(Specialization::className(), ['SpecializationCode' => 'Specialization']);
    }
	
		 public function getJobsItem()
    {
        return $this->hasOne(Jobs::className(), ['JobCode' => 'JobCode']);
    }
	 
	 
	 
    public function attributeLabels()
    {
        return [
            'EmployeeId' => 'Employee ID',
			'Name' => 'Name',
            'LastName' => 'Last Name',
            'Phone' => 'Phone',
            'DateOfBirth' => 'Date Of Birth',
            'DateOfStartPosition' => 'Date Of Start Position',
            'Specialization' => 'Specialization',
            'JobCode' => 'Job Code',
			'UserId' => 'User system ID',
        ];
    }
		public static function getEmployees()
	{
		$allEmployees = self::find()->all();
		$allEmployeesArray = ArrayHelper::
					map($allEmployees, 'EmployeeId', 'Name');
		return $allEmployeesArray;						
	}
}
