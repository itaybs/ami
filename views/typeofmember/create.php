<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Typeofmember */

$this->title = 'Create Typeofmember';
$this->params['breadcrumbs'][] = ['label' => 'Typeofmembers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="typeofmember-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
