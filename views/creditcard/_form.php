<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Members;

/* @var $this yii\web\View */
/* @var $model app\models\Creditcard */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="creditcard-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'NumOfCard')->textInput() ?>

    <?= $form->field($model, 'NameOfOwner')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Validity')->textInput() ?>

    <?//= $form->field($model, 'IdOfMember')->textInput() ?>
	
	<?= $form->field($model, 'IdOfMember')->
				dropDownList(Members::getAllMembers()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
