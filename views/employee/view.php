<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Employee */

$this->title = $model->Name;
$this->params['breadcrumbs'][] = ['label' => 'Employees', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employee-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->EmployeeId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->EmployeeId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'EmployeeId',
            'UserId',
            'Name',
            'LastName',
            'Phone',
            'DateOfBirth',
            'DateOfStartPosition',
			[ // the Specialization name 
				'label' => $model->attributeLabels()['Specialization'],
				'value' => $model->specializationItem->Description,	
			],
			[ // the JobCode name 
				'label' => $model->attributeLabels()['JobCode'],
				'value' => $model->jobsItem->Description,	
			],
        ],
    ]) ?>

</div>
