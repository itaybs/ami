<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EmployeeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Employees';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employee-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Employee', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            
            //'UserId',
            'Name',
            'LastName',
			'EmployeeId',
            'Phone',
            // 'DateOfBirth',
            // 'DateOfStartPosition',
			[
				'attribute' => 'Specialization',
				'label' => 'Specialization',
				'format' => 'raw',
				'value' => function($model){
					return $model->specializationItem->Description;
				},
				'filter'=>Html::dropDownList('EmployeeSearch[Specialization]', $Specialization, $Specializations, ['class'=>'form-control']),
			],
			[
				'attribute' =>  'JobCode',
				'label' => 'Job Code',
				'format' => 'raw',
				'value' => function($model){
					return $model->jobsItem->Description;
				},
				'filter'=>Html::dropDownList('EmployeeSearch[JobCode]', $JobCode, $Jobs, ['class'=>'form-control']),
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
