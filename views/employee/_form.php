<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Specialization;
use app\models\Jobs;


/* @var $this yii\web\View */
/* @var $model app\models\Employee */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="employee-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'EmployeeId')->textInput() ?>

   
	<?= $form->field($model, 'UserId')->textInput() ?>

    <?= $form->field($model, 'Name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'LastName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Phone')->textInput() ?>

    <?= $form->field($model, 'DateOfBirth')->textInput() ?>

    <?= $form->field($model, 'DateOfStartPosition')->textInput() ?>

    	
	<?= $form->field($model, 'Specialization')->
				dropDownList(Specialization::getSpecializations()) ?>
				
	<?= $form->field($model, 'JobCode')->
				dropDownList(Jobs::getJobs()) ?>

				
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
