<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SubscriptionProgram */

$this->title = 'Update Subscription Program: ' . $model->NumOfProgram;
$this->params['breadcrumbs'][] = ['label' => 'Subscription Programs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->NumOfProgram, 'url' => ['view', 'NumOfProgram' => $model->NumOfProgram, 'Id' => $model->Id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="subscription-program-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
