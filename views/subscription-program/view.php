<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SubscriptionProgram */

$this->title = $model->NumOfProgram;
$this->params['breadcrumbs'][] = ['label' => 'Subscription Programs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscription-program-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'NumOfProgram' => $model->NumOfProgram, 'Id' => $model->Id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'NumOfProgram' => $model->NumOfProgram, 'Id' => $model->Id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
			[ // the CodeOfActivity name 
				'label' => $model->attributeLabels()['NumOfProgram'],
				'value' => $model->numOfProgramItem->Description,	
			],
            'Id',
        ],
    ]) ?>

</div>
