<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\GroupactivitySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="groupactivity-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'CodeOfActivity') ?>

    <?= $form->field($model, 'Day') ?>

    <?= $form->field($model, 'Time') ?>
	<?= $form->field($model, 'nameOfActivity') ?>
    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
