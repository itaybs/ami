<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Groupactivity */

$this->title = 'Create Groupactivity';
$this->params['breadcrumbs'][] = ['label' => 'Groupactivities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="groupactivity-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
