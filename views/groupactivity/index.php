<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GroupactivitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Groupactivities';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="groupactivity-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Groupactivity', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

			[
				'attribute' => 'CodeOfActivity',
				'label' => 'Code Of Activity',
				'format' => 'raw',
				'value' => function($model){
					return $model->codeOfActivityItem->Description;
				},
				'filter'=>Html::dropDownList('GroupactivitySearch[CodeOfActivity]', $CodeOfActivity, $Specializations, ['class'=>'form-control']),
			],
			'nameOfActivity',
            'Day',
            'Time',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
