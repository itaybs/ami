<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Groupactivity */

$this->title = 'Update Groupactivity: ' . $model->CodeOfActivity;
$this->params['breadcrumbs'][] = ['label' => 'Groupactivities', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->CodeOfActivity, 'url' => ['view', 'id' => $model->CodeOfActivity]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="groupactivity-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
