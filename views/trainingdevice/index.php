<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TrainingdeviceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Trainingdevices';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trainingdevice-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Trainingdevice', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'TrainingDeviceCode',
            'Description',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
