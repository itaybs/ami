<?php

/* @var $this yii\web\View */

$this->title = 'AMI GYM';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>AMI GYM</h1>

        <p class="lead">Stopping technological progress to save money is like stopping your watch to save time.</p>
            <p2>If you are not already logged in, click on the button below</p2>
        <p><a class="btn btn-lg btn-success" href="http://itaybs.myweb.jce.ac.il/amiproject/web/site/login">Get started with AMI GYM</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Itay Basteker</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-lg btn-info" href="http://www.yiiframework.com/doc/">manager1 &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Aviv Atias</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-lg btn-info" href="http://www.yiiframework.com/forum/">manager2 &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Mor Ben-Abu</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-lg btn-info" href="http://www.yiiframework.com/extensions/">clerk &raquo;</a></p>
            </div>
        </div>

    </div>
</div>
