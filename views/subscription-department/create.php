<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SubscriptionDepartment */

$this->title = 'Create Subscription Department';
$this->params['breadcrumbs'][] = ['label' => 'Subscription Departments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscription-department-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
