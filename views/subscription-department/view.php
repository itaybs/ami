<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SubscriptionDepartment */

$this->title = $model->CodeOfActivity;
$this->params['breadcrumbs'][] = ['label' => 'Subscription Departments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscription-department-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'CodeOfActivity' => $model->CodeOfActivity, 'Id' => $model->Id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'CodeOfActivity' => $model->CodeOfActivity, 'Id' => $model->Id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
			[ // the CodeOfActivity name 
				'label' => $model->attributeLabels()['CodeOfActivity'],
				'value' => $model->codeOfActivityItem->Description,	
			],
            'Id',
        ],
    ]) ?>

</div>
