<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Members;
use app\models\Groupactivity;

/* @var $this yii\web\View */
/* @var $model app\models\SubscriptionDepartment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="subscription-department-form">

    <?php $form = ActiveForm::begin(); ?>

    <?//= $form->field($model, 'CodeOfActivity')->textInput() ?>

    <?//= $form->field($model, 'Id')->textInput() ?>
	
	<?= $form->field($model, 'CodeOfActivity')->
				dropDownList(Groupactivity::getAllActivities()) ?>
				
	<?= $form->field($model, 'Id')->
				dropDownList(Members::getAllMembers()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
