<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PresenceSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="presence-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'EmployeeId') ?>

    <?= $form->field($model, 'Date') ?>

    <?= $form->field($model, 'TimeOfStart') ?>

    <?= $form->field($model, 'TimeOfFinish') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
